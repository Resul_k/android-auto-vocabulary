package android.auto.com.autodroid.auto.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Handles read action
 * Created by resulkocyigit on 17/01/15.
 */
public class AutoMessageReadReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AUTO", "read");

    }
}
