package android.auto.com.autodroid.navigation;

import android.app.FragmentManager;
import android.auto.com.autodroid.R;
import android.auto.com.autodroid.gui.SmsFragment;

public class NavigationImpl implements Navigation{

    private FragmentManager fragmentManager;

    public NavigationImpl(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void sendSms() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, SmsFragment.newInstance(2))
                .commit();
    }

    @Override
    public void playMusic() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, SmsFragment.newInstance(3))
                .commit();
    }

    @Override
    public void startVocabulary() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, SmsFragment.newInstance(1))
                .commit();
    }
}
