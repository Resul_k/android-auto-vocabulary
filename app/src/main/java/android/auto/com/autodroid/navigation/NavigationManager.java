package android.auto.com.autodroid.navigation;

import android.app.FragmentManager;

/**
 * Handles navigation and forwarding to fragments
 * Created by resulkocyigit on 16/01/15.
 */
public class NavigationManager{

    public static final int VOCABULARY = 0;
    public static final int SMS = 1;
    public static final int MUSIC = 2;

    private Navigation navigation;

    public NavigationManager(FragmentManager fragmentManager){
        navigation = new NavigationImpl(fragmentManager);
    }

    public void navigateTo(int position){
        switch(position){
            case NavigationManager.SMS:
                navigation.sendSms();
                break;
            case NavigationManager.MUSIC:
                navigation.playMusic();
                break;
            case NavigationManager.VOCABULARY:
                navigation.startVocabulary();
                break;
            default:
        }
    }
}
