package android.auto.com.autodroid.app;

import android.app.Application;

/**
 * Created by resulkocyigit on 17/01/15.
 */
public class AutoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AutoApp.initInstance(this);
        AutoApp.getInstance().initVocabulary();
    }

}
