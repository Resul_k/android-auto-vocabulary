package android.auto.com.autodroid.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.auto.com.autodroid.auto.AutoMessage;
import android.auto.com.autodroid.notification.AutoNotificationManager;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * IntentService to send new vocables
 * Created by resulkocyigit on 28/01/15.
 */
public class VocabularyService extends IntentService{

    public VocabularyService(){
        super("");

    }

    public VocabularyService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent != null && intent.getExtras() != null){

            AutoMessage autoMessage = (AutoMessage)intent.getExtras().getParcelable(AutoNotificationManager.ACTION_MESSAGE_REPLY);
            if(autoMessage == null){
                return;
            }

            AutoNotificationManager nm = new AutoNotificationManager();
            PendingIntent readPendingIntent = nm.getReadPendingIntent(autoMessage.getId());
            PendingIntent replyPendingIntent = nm.getReplyPendingIntent(autoMessage.getId());
            NotificationCompat.CarExtender.UnreadConversation.Builder unreadConversation = nm.buildSingleUnreadConversation(readPendingIntent, replyPendingIntent, autoMessage);
            nm.sendAutoNotification(autoMessage, unreadConversation, readPendingIntent);
        }
    }
}
