package android.auto.com.autodroid;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.auto.com.autodroid.app.AutoApp;
import android.auto.com.autodroid.auto.AutoMessage;
import android.auto.com.autodroid.auto.receivers.AutoMessageReadReceiver;
import android.auto.com.autodroid.auto.receivers.AutoMessageReplyReceiver;
import android.auto.com.autodroid.navigation.NavigationManager;
import android.auto.com.autodroid.notification.AutoNotificationManager;
import android.auto.com.autodroid.service.VocabularyService;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class AutoActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private NavigationManager navigationManager;
    private AutoNotificationManager autoNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        navigationManager = new NavigationManager(getFragmentManager());
        //onNavigationDrawerItemSelected(NavigationManager.SMS);
    }

    private AutoMessageReadReceiver autoMessageReadReceiver;
    private AutoMessageReplyReceiver autoMessageReplyReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        autoNotificationManager = new AutoNotificationManager();

        IntentFilter autoReadFilter = new IntentFilter();
        autoReadFilter.addAction(AutoNotificationManager.ACTION_MESSAGE_HEARD);
        autoMessageReadReceiver = new AutoMessageReadReceiver();
        registerReceiver(autoMessageReadReceiver, autoReadFilter);

        IntentFilter autoReplyFilter = new IntentFilter();
        autoReadFilter.addAction(AutoNotificationManager.ACTION_MESSAGE_REPLY);
        autoMessageReplyReceiver = new AutoMessageReplyReceiver();
        registerReceiver(autoMessageReplyReceiver, autoReplyFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(autoMessageReadReceiver);
        unregisterReceiver(autoMessageReplyReceiver);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if(navigationManager != null){
            navigationManager.navigateTo(position);
            mNavigationDrawerFragment.selectNavigationItem(position);
        }
    }

    public void onSectionAttached(int number) {
        if(autoNotificationManager == null){
            autoNotificationManager = new AutoNotificationManager();
        }
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                AutoMessage autoMessage = AutoApp.getInstance().getStart();

                if(autoMessage == null){
                    return;
                }

                Intent newIntent = new Intent(this, VocabularyService.class);
                newIntent.putExtra(AutoNotificationManager.ACTION_MESSAGE_REPLY,autoMessage);
                startService(newIntent);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);

                break;
            case 3:
                mTitle = getString(R.string.title_section3);

                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.auto, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class SmsFragment extends Fragment {
        private static final String ARG_KEY_VALUE = "args.key.value";

        private int position;

        public SmsFragment newInstance(int sectionNumber) {
            SmsFragment fragment = new SmsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_KEY_VALUE, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public SmsFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Bundle extras = getArguments();
            position = 0;
            if(extras != null){
                position = extras.getInt(ARG_KEY_VALUE);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_auto, container, false);
            TextView text = null;
            if(rootView != null){
                text = (TextView)rootView.findViewById(R.id.text);
                text.setText(""+position);
            }
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((AutoActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_KEY_VALUE));
        }
    }

}
