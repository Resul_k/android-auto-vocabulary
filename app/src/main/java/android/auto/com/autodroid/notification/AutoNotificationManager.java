package android.auto.com.autodroid.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.auto.com.autodroid.R;
import android.auto.com.autodroid.app.AutoApp;
import android.auto.com.autodroid.auto.AutoMessage;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;

/**
 * Handles notifications
 * Created by resulkocyigit on 17/01/15.
 */
public class AutoNotificationManager {

    public static final String EXTRA_VOICE_REPLY = "extra.voice.reply";
    public static final String KEY_CONVERSATION_ID = "key.auto.conversation.id";
    public static final String ACTION_MESSAGE_HEARD = "android.auto.com.autodroid.notification.ACTION_MESSAGE_READ";
    public static final String ACTION_MESSAGE_REPLY = "android.auto.com.autodroid.notification.ACTION_MESSAGE_REPLY";

    private RemoteInput remoteInput;

    public AutoNotificationManager(){
        String replyLabel = AutoApp.getInstance().getApplicationContext().getString(R.string.notification_reply_label);
        remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)
                .setLabel(replyLabel)
                .build();
    }

    public UnreadConversation.Builder buildSingleUnreadConversation(PendingIntent read,
                                                PendingIntent reply, AutoMessage autoMessage){
        UnreadConversation.Builder unreadConvBuilder =
                new NotificationCompat.CarExtender.UnreadConversation
                        .Builder(autoMessage.getMessageTitle())
                        .setReadPendingIntent(read)
                        .setReplyAction(reply, remoteInput);
        unreadConvBuilder.addMessage(autoMessage.getMessage())
                .setLatestTimestamp(System.currentTimeMillis());

        return unreadConvBuilder;
    }

    public PendingIntent getReadPendingIntent(int conversationId){
        Intent intent = new Intent()
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .setAction(ACTION_MESSAGE_HEARD)
                .putExtra(KEY_CONVERSATION_ID, conversationId);
                return PendingIntent.getBroadcast(AutoApp.getInstance().getApplicationContext(),
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public PendingIntent getReplyPendingIntent(int conversationId){
        Intent intent = new Intent()
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .setAction(ACTION_MESSAGE_REPLY)
                .putExtra(KEY_CONVERSATION_ID, conversationId);

        return PendingIntent.getBroadcast(
                AutoApp.getInstance().getApplicationContext(),
                1,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void removeConversation(Context context, int conversationId){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(conversationId);
    }

    public long sendAutoNotification(AutoMessage message,
                                     UnreadConversation.Builder unreadConversation,
                                     PendingIntent readPendingIntent){

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(AutoApp.getInstance().getApplicationContext())
                        .setSmallIcon(R.drawable.gdglogo)
                        .setContentText(message.getNotificationText())
                        .setWhen(System.currentTimeMillis())
                        .setContentTitle(message.getNotificationTitle())
                        .setContentIntent(readPendingIntent);

        notificationBuilder.extend(new NotificationCompat.CarExtender()
                .setUnreadConversation(unreadConversation.build()));
        NotificationManagerCompat msgNotificationManager =
                NotificationManagerCompat.from(AutoApp.getInstance().getApplicationContext());
        msgNotificationManager.notify(message.getId(), notificationBuilder.build());

        return message.getId();
    }
}
