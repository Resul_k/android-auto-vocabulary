package android.auto.com.autodroid.app;

import android.auto.com.autodroid.R;
import android.auto.com.autodroid.auto.AutoMessage;
import android.content.Context;
import android.content.res.Resources;

import java.util.HashMap;
import java.util.Random;

/**
 * Application class as Singleton
 * Created by resulkocyigit on 17/01/15.
 */
public class AutoApp {

    private static AutoApp INSTANCE;
    private AutoApplication application;
    private Random random = new Random();
    private HashMap<Integer,AutoMessage> vocabulary = new HashMap<Integer,AutoMessage>();
    private HashMap<Integer, Integer> vocabularyIndex = new HashMap<Integer, Integer>();

    private AutoApp(AutoApplication application) {
        this.application = application;
    }

    public static void initInstance(final AutoApplication application) {
        if (INSTANCE == null && application != null) {
            INSTANCE = new AutoApp(application);
        }
    }

    private int nextRandomConversationId(){
        return random.nextInt();
    }

    public void initVocabulary() {

        Resources res = getApplicationContext().getResources();
        String[] englishVocabulary = res.getStringArray(R.array.english);

        vocabulary.clear();

        int i=0;
        for (String vocable : englishVocabulary) {
            int conversationId = nextRandomConversationId();
            AutoMessage autoMessage = new AutoMessage();
            autoMessage.setId(conversationId);
            autoMessage.setMessage(vocable);
            autoMessage.setMessageTitle("your trainer. Please repeat... ");
            autoMessage.setNotificationTitle("English training");
            autoMessage.setNotificationText("Chapter one.");
            vocabulary.put(conversationId, autoMessage);
            vocabularyIndex.put(i,conversationId);
            i++;
        }

    }

    public AutoMessage getStart(){
        int conversationId = nextRandomConversationId();
        AutoMessage autoMessage = new AutoMessage();
        autoMessage.setId(conversationId);
        autoMessage.setMessage("... to Android Auto. Reply to this message to begin your vocabulary training.");
        autoMessage.setMessageTitle("your GDG Düsseldorf. Welcome ... ");
        autoMessage.setNotificationTitle("English training");
        autoMessage.setNotificationText("Chapter one.");

        return autoMessage;
    }

    public AutoMessage getVocable(int conversationId){
        return vocabulary.get(conversationId);
    }
    public AutoMessage getRandomVocable(){

        if(vocabulary.size() == 0){
            return null;
        }

        int randomIndex = random.nextInt(vocabulary.size());
        return vocabulary.get(vocabularyIndex.get(randomIndex));
    }

    public static AutoApp getInstance() {
        return INSTANCE;
    }

    public Context getApplicationContext() {
        return application.getApplicationContext();
    }
}
