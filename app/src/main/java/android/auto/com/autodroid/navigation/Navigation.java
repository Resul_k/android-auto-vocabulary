package android.auto.com.autodroid.navigation;

/**
 * Command pattern
 * Created by resulkocyigit on 16/01/15.
 */
public interface Navigation {
    public void sendSms();
    public void playMusic();
    public void startVocabulary();
}
