package android.auto.com.autodroid.auto.receivers;

import android.auto.com.autodroid.app.AutoApp;
import android.auto.com.autodroid.auto.AutoMessage;
import android.auto.com.autodroid.notification.AutoNotificationManager;
import android.auto.com.autodroid.service.VocabularyService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;

/**
 * Handles action for reply
 * Created by resulkocyigit on 17/01/15.
 */
public class AutoMessageReplyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null) {

            int conversationId = intent.getIntExtra(AutoNotificationManager.KEY_CONVERSATION_ID, -1);

            String replyMessage = (String) getMessageText(intent);

            AutoNotificationManager nm = new AutoNotificationManager();

            AutoMessage autoMessage = AutoApp.getInstance().getVocable(conversationId);
            if ( autoMessage != null) {

                //if replyMessage equals autoMessage, then check that message, so it will not
                //get as new vocable and remove notification only if reply was correct

                nm.removeConversation(context, conversationId);
            }

            autoMessage = AutoApp.getInstance().getRandomVocable();

            if (autoMessage == null) {
                return;
            }

            Intent newIntent = new Intent(context, VocabularyService.class);
            newIntent.putExtra(AutoNotificationManager.ACTION_MESSAGE_REPLY,autoMessage);
            context.startService(newIntent);
        }
    }

    /**
     * Get the message text from the intent.
     * Note that you should call
     * RemoteInput.getResultsFromIntent() to process
     * the RemoteInput.
     */
    private CharSequence getMessageText(Intent intent) {
        Bundle remoteInput =
                RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getCharSequence(AutoNotificationManager.EXTRA_VOICE_REPLY);
        }
        return null;
    }
}
