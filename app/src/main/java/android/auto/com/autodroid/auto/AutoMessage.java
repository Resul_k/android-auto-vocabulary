package android.auto.com.autodroid.auto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Message to trans
 * Created by resulkocyigit on 25/01/15.
 */
public class AutoMessage implements Parcelable {

    private int id;
    private boolean check;
    private String messageTitle;
    private String message;

    private String notificationTitle;
    private String notificationText;

    public AutoMessage() {
    }

    public boolean isChecked(){
        return check;
    }

    public void hasChecked(boolean check){
        this.check = check;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeByte(check ? (byte) 1 : (byte) 0);
        dest.writeString(this.messageTitle);
        dest.writeString(this.message);
        dest.writeString(this.notificationTitle);
        dest.writeString(this.notificationText);
    }

    private AutoMessage(Parcel in) {
        this.id = in.readInt();
        this.check = in.readByte() != 0;
        this.messageTitle = in.readString();
        this.message = in.readString();
        this.notificationTitle = in.readString();
        this.notificationText = in.readString();
    }

    public static final Parcelable.Creator<AutoMessage> CREATOR = new Parcelable.Creator<AutoMessage>() {
        public AutoMessage createFromParcel(Parcel source) {
            return new AutoMessage(source);
        }

        public AutoMessage[] newArray(int size) {
            return new AutoMessage[size];
        }
    };
}
