package android.auto.com.autodroid.gui;

import android.app.Activity;
import android.app.Fragment;
import android.auto.com.autodroid.AutoActivity;
import android.auto.com.autodroid.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SmsFragment extends Fragment {
    private static final String ARG_KEY_VALUE = "args.key.value";

    private int position;

    public static SmsFragment newInstance(int sectionNumber) {
        SmsFragment fragment = new SmsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_VALUE, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SmsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();
        position = 0;
        if(extras != null){
            position = extras.getInt(ARG_KEY_VALUE);
            ((AutoActivity)getActivity()).onSectionAttached(position);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_auto, container, false);
        TextView text = null;
        if(rootView != null){
            text = (TextView)rootView.findViewById(R.id.text);
            text.setText(""+position);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
